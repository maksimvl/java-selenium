package org.example;


import org.example.common.SeleniumTest;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SeleniumHomePageTest extends SeleniumTest {

    @Test
    public void can_go_to_home_page() {
        assertThat(homePage.isAt(), is(true));
    }
}

