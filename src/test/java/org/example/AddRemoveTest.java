package org.example;

import org.example.common.SeleniumTest;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class AddRemoveTest extends SeleniumTest {

    @Test
    public void can_go_to_home_page() {
        assertThat(homePage.isAt(), is(true));
    }

    @Test
    public void can_go_to_addRemove_page() {
        AddRemoveElements addRemoveElements = homePage.goToAddRemovePage();
        assertThat(addRemoveElements.isAtAddRemovePage(), is(true));
    }

    @Test
    public void can_add_element() {
        AddRemoveElements addRemoveElements = homePage.goToAddRemovePage();
        addRemoveElements.clickAdd();
        assertThat(addRemoveElements.buttonIsPresented(), is(true));
    }

    @Test
    public void can_add_10_elements() {
        AddRemoveElements addRemoveElements = homePage.goToAddRemovePage();
        addRemoveElements.click10Times();
        assertThat(addRemoveElements.countDeleteButtons(), is(10));
    }

    @Test
    public void can_delete_5_added_element() {
        AddRemoveElements addRemoveElements = homePage.goToAddRemovePage();
        addRemoveElements.click10Times();
        addRemoveElements.clickRemove5Times();
        assertThat(addRemoveElements.countDeleteButtons(), is(5));
    }
}


