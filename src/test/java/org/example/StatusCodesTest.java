package org.example;

import org.example.common.SeleniumTest;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StatusCodesTest extends SeleniumTest {

    @Test
    public void can_go_to_home_page() {
        assertThat(homePage.isAt(), is(true));
    }

    @Test
    public void can_go_to_statusCodes_page() {
        StatusCodes statusCodes = homePage.goToStatusCodesPage();
        assertThat(statusCodes.isAtStatusCodesPage(), is(true));
    }

    @Test
    public void go_to_200Page() {
        StatusCodes statusCodes = homePage.goToStatusCodesPage();
        statusCodes.clickButton200();
        assertThat(statusCodes.page200IsPresented(), is(true));
    }

    @Test
    public void go_to_301Page() {
        StatusCodes statusCodes = homePage.goToStatusCodesPage();
        statusCodes.clickButton301();
        assertThat(statusCodes.page301IsPresented(), is(true));
    }

    @Test
    public void go_to_404Page() {
        StatusCodes statusCodes = homePage.goToStatusCodesPage();
        statusCodes.clickButton404();
        assertThat(statusCodes.page404IsPresented(), is(true));
    }

    @Test
    public void go_to_500Page() {
        StatusCodes statusCodes = homePage.goToStatusCodesPage();
        statusCodes.clickButton500();
        assertThat(statusCodes.page500IsPresented(), is(true));
    }
}
