package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AddRemoveElements extends Page {

    private final By deleteButton = By.xpath("//button[@onclick='deleteElement()']");
    private final By addRemoveTitle = By.tagName("h3");
    private final By addButton = By.xpath("//button[@onclick='addElement()']");

    public AddRemoveElements(WebDriver driver) {
        super(driver);
    }

    public boolean isAtAddRemovePage() {
        return driver.findElement(addRemoveTitle).getText().equals("Add/Remove Elements");
    }

    public void clickAdd() {
        driver.findElement(addButton).click();
    }

    public boolean buttonIsPresented() {
        return driver.findElement(deleteButton).getText().equals("Delete");
    }

    public void click10Times() {
        for (int i = 0; i < 10; i++) {
            driver.findElement(addButton).click();
            new WebDriverWait(driver, 2).until(ExpectedConditions.presenceOfElementLocated(addButton));
        }

    }

    public void clickRemove5Times() {
        new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(deleteButton));
        for (int i = 0; i < 5; i++) {
            driver.findElement(deleteButton).click();
        }
    }

    public int countDeleteButtons() {
        int xpathCount = driver.findElements(deleteButton).size();
        return xpathCount;
    }


}

