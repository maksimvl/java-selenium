package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends Page {
    private final By mainTitle = By.tagName("h1");
    private final By addRemoveLink = By.linkText("Add/Remove Elements");
    private final By statusCodeLink = By.linkText("Status Codes");


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get("https://the-internet.herokuapp.com");
    }


    public boolean isAt() {
        return driver.findElement(mainTitle).getText().equals("Welcome to the-internet");
    }


    public AddRemoveElements goToAddRemovePage() {
        driver.findElement(addRemoveLink).click();
        return new AddRemoveElements(driver);
    }

    public StatusCodes goToStatusCodesPage() {
        driver.findElement(statusCodeLink).click();
        return new StatusCodes(driver);
    }
}
