package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StatusCodes extends Page{

    private final By Button200 = By.xpath("//a[@href='status_codes/200']");
    private final By Button301 = By.xpath("//a[@href='status_codes/301']");
    private final By Button404 = By.xpath("//a[@href='status_codes/404']");
    private final By Button500 = By.xpath("//a[@href='status_codes/500']");
    private final By StatusCodesTitle = By.tagName("h3");
    private final By StatusCodes200 = By.tagName("p");
    private final By StatusCodes301 = By.tagName("p");
    private final By StatusCodes404 = By.tagName("p");
    private final By StatusCodes500 = By.tagName("p");

    public StatusCodes(WebDriver driver) {
        super(driver);
    }

    public boolean isAtStatusCodesPage() {
        return driver.findElement(StatusCodesTitle).getText().equals("Status Codes");
    }

    public void clickButton200() {
        driver.findElement(Button200).click();
    }

    public void clickButton301() {
        driver.findElement(Button301).click();
    }

    public void clickButton404() {
        driver.findElement(Button404).click();
    }

    public void clickButton500() {
        driver.findElement(Button500).click();
    }

    public boolean page200IsPresented() {
        return driver.findElement(StatusCodes200).getText().contains("This page returned a 200 status code.");
    }

    public boolean page301IsPresented() {
        return driver.findElement(StatusCodes301).getText().contains("This page returned a 301 status code.");
    }

    public boolean page404IsPresented() {
        return driver.findElement(StatusCodes404).getText().contains("This page returned a 404 status code.");
    }

    public boolean page500IsPresented() {
        return driver.findElement(StatusCodes500).getText().contains("This page returned a 500 status code.");
    }

}
